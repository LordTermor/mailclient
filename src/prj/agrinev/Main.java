package prj.agrinev;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {


    static String NAME;
    static String PASS;

    static String POP3_HOSTNAME;
    static int POP3_PORT;
    static String SMTP_HOSTNAME;
    static int SMTP_PORT;


    public static void main(String[] args) {
        launch(args);
    }

    @FXML
    private TextField loginTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private TextField pop3textField;
    @FXML
    private TextField smtpTextField;

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
        Parent root = FXMLLoader.load(getClass().getResource("LoginWindow.fxml"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    @FXML
    void onLoginButtonAction(ActionEvent event) throws IOException {

        NAME = loginTextField.getText();
        PASS = passwordTextField.getText();

        String[] tmp = pop3textField.getText().split(":");
        POP3_HOSTNAME = tmp[0];
        POP3_PORT = Integer.valueOf(tmp[1]);

        tmp = smtpTextField.getText().split(":");
        SMTP_HOSTNAME = tmp[0];
        SMTP_PORT = Integer.valueOf(tmp[1]);

        loginTextField.getScene().getWindow().hide();

        Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        Stage primaryStage = new Stage();
        primaryStage.setTitle("POP3 Example");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }
}
