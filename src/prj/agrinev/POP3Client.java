package prj.agrinev;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class POP3Client {

    private static Pattern contentType = Pattern.compile("Content-Type:\\s*(\\S+)(;|\\n|\\Z)", Pattern.CASE_INSENSITIVE);
    private static Pattern encoding = Pattern.compile("Content-Transfer-Encoding:\\s*(\\S+)(;|\\n|\\Z)", Pattern.CASE_INSENSITIVE);
    private String lastMessage;
    private PrintWriter socketOutputStream;
    private BufferedReader socketInputStream;
    private String host;
    private int port;
    private String authUserName;
    private String authUserPass;

    POP3Client(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private void initSocket() throws IOException {
        SSLSocket socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(host, port);
        socket.startHandshake();
        socketOutputStream = new PrintWriter(socket.getOutputStream(), true);
        socketInputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        lastMessage = socketInputStream.readLine();
        socketSend("USER " + authUserName);
        socketSend("PASS " + authUserPass);

    }

    public void setAuthData(String name, String pass) {
        authUserName = name;
        authUserPass = pass;
    }

    ArrayList<MessageHeader> getInbox() throws IOException {
        initSocket();
        ArrayList<MessageHeader> inbox = new ArrayList<>();
        socketSend("STAT");
        int inboxSize = Integer.parseInt(lastMessage.split(" ")[1]);
        for (int i = 1; i <= inboxSize; i++) {
            socketSend("RETR " + i);
            String result;
            MessageHeader header = new MessageHeader();
            while (!(result = socketInputStream.readLine()).isEmpty()) {
                String decoded = Decoder.decodeMime(result);

                if (decoded.startsWith("Subject: ")) {
                    header.mSubject = decoded.substring("Subject: ".length());
                } else if (decoded.startsWith("From: ")) {
                    header.mFrom = decoded.substring("From: ".length());
                } else if (decoded.startsWith("To: ")) {
                    header.mTo = decoded.substring("To: ".length());
                }
            }
            //noinspection StatementWithEmptyBody
            while (!socketInputStream.readLine().equals(".")) {

            }
            inbox.add(header);
        }
        socketSend("QUIT");

        return inbox;
    }

    public Message getMessage(int index) throws IOException {
        initSocket();

        socketSend("RETR " + (index + 1));

        String result, boundary = "";
        MessageHeader header = new MessageHeader();
        while (!(result = socketInputStream.readLine()).isEmpty()) {
            String decoded = Decoder.decodeMime(result);

            if (decoded.startsWith("Subject: ")) {
                header.mSubject = decoded.substring("Subject: ".length());
            } else if (decoded.startsWith("From: ")) {
                header.mFrom = decoded.substring("From: ".length());
            } else if (decoded.startsWith("To: ")) {
                header.mTo = decoded.substring("To: ".length());
            }
            int ind;

            Pattern pattern = Pattern.compile("boundary=\"?(\\S+)\"?", Pattern.CASE_INSENSITIVE);

            Matcher matcher = pattern.matcher(decoded);
            if (matcher.find()) {
                boundary = matcher.group(1);
                if (boundary.endsWith("\"")) {
                    boundary = boundary.substring(0, boundary.length() - 1);
                }
            }
        }

        StringBuilder bldr = new StringBuilder();
        while (!(result = socketInputStream.readLine()).equals(".")) {
            bldr.append(Decoder.decodeMime(result));
            bldr.append("\n");
        }
        Message msg = new Message();
        msg.mHeader = header;

        if (!boundary.isEmpty()) {
            String[] attaches = bldr.toString().split(Pattern.quote("--" + boundary + "\n"));

            StringBuilder bodyBuilder = new StringBuilder();

            for (int i = 1; i < attaches.length; i++) {
                Attachment attachment = parseAttachment(attaches[i]);

                if (attachment.type.startsWith("text/plain")) {
                    bodyBuilder.append(new String(attachment.body));
                } else {
                    msg.mAttachments.add(attachment);
                }

            }
            msg.mBody = bodyBuilder.toString();

        } else {
            msg.mBody = bldr.toString();
        }

        socketSend("QUIT");
        return msg;
    }

    private void socketSend(String command) throws IOException {
        socketOutputStream.write(command + "\r\n");
        System.out.println("POP3: CLIENT: "+command);
        socketOutputStream.flush();
        lastMessage = socketInputStream.readLine();
        System.out.println("POP3: SERVER: "+lastMessage);

    }

    private Attachment parseAttachment(String attachment) {
        Attachment result = new Attachment();

        String[] splittedAttachment = attachment.split("\n\n");

        String header = splittedAttachment[0];

        Matcher matcherContent = contentType.matcher(header);
        Matcher matcherEncoding = encoding.matcher(header);


        if (matcherContent.find()) {
            result.type = matcherContent.group(1);
        }
        if (matcherEncoding.find()) {
            if (matcherEncoding.group(1).toLowerCase().equals("base64")) {
                try {
                    result.body = Base64.getDecoder().decode(splittedAttachment[1].replace("\n", ""));
                } catch (Exception e){
                    result.body = splittedAttachment[1].replace("\n", "").getBytes();
                }
            }
        } else {
            result.body = splittedAttachment[1].replace("\n", "").getBytes();

        }
        return result;
    }

    static private class Decoder {
        static Pattern pattern = Pattern.compile("=\\?(\\S+)\\?(\\S+)\\?(\\S+)\\?=", Pattern.CASE_INSENSITIVE);

        private Decoder() {
        }

        static String decodeMime(String input) {
            Matcher m = pattern.matcher(input);
            if (m.find()) {
                String resultString = "";

                switch (m.group(2).toLowerCase()) {

                    case "b":
                        try {
                            resultString = new String(Base64.getDecoder().decode(m.group(3)));
                        } catch (Exception e) {
                            resultString = "";
                        }
                        break;
                    case "q":

                        byte[] buffer = m.group(3).getBytes();
                        int rlength = QuotedPrintable.decode(buffer);
                        resultString = new String(buffer).substring(0, rlength);
                        break;

                }
                return input.replace(m.group(0), resultString);
            }
            return input;
        }
    }

    public class MessageHeader {
        private String mFrom;
        private String mTo;
        private String mSubject;

        public String getFrom() {
            return mFrom;
        }

        public String getTo() {
            return mTo;
        }


        public String getSubject() {
            return mSubject;
        }

    }

    public class Message {
        private MessageHeader mHeader;
        private String mBody;
        private ArrayList<Attachment> mAttachments;

        public Message() {
            mAttachments = new ArrayList<>();
        }


        public MessageHeader getHeader() {
            return mHeader;
        }

        public String getBody() {
            return mBody;
        }

        public ArrayList<Attachment> getAttachments() {
            return mAttachments;
        }
    }

    public class Attachment {
        private String type = "";
        private byte[] body;

        public String getType() {
            return type;
        }

        public byte[] getBody() {
            return body;
        }
    }

}
