package prj.agrinev;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class MainController {


    ArrayList<POP3Client.MessageHeader> inbox;
    @FXML
    private Label fromLabel;
    @FXML
    private Label toLabel;
    @FXML
    private Label subjectLabel;
    @FXML
    private TextArea bodyLabel;
    @FXML
    private ListView<String> attachmentsListView;
    @FXML
    private ListView<String> inboxListView;
    private POP3Client pop3Client;

    {
        try {
            pop3Client = new POP3Client(Main.POP3_HOSTNAME, Main.POP3_PORT);
            pop3Client.setAuthData(Main.NAME, Main.PASS);

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    @FXML
    void initialize() throws Exception {
        inbox = pop3Client.getInbox();

        ArrayList<String> subjectsList = new ArrayList<>(inbox.size());

        for (int i = 0; i < inbox.size(); i++) {

            subjectsList.add(i,
                    "From: " + inbox.get(i).getFrom() + "\n" +
                            "To: " + inbox.get(i).getTo() + "\n" +
                            (inbox.get(i).getSubject().isEmpty() ? "==No subject==" : inbox.get(i).getSubject()));
        }

        ObservableList<String> items = FXCollections.observableArrayList(subjectsList);
        inboxListView.setItems(items);

        inboxListView.setOnMouseClicked((event -> {
            try {
                POP3Client.Message msg = pop3Client.getMessage(inboxListView.getSelectionModel().getSelectedIndex());
                subjectLabel.setText("Subject: " + msg.getHeader().getSubject());
                fromLabel.setText("From: " + msg.getHeader().getFrom());
                toLabel.setText("To: " + msg.getHeader().getTo());
                bodyLabel.setText(msg.getBody());
                attachmentsListView.getItems().clear();

                ArrayList<POP3Client.Attachment> attachments = msg.getAttachments();
                for (int i = 0; i < attachments.size(); i++) {
                    POP3Client.Attachment el = attachments.get(i);
                    if (!el.getType().equals("text/plain"))
                        attachmentsListView.getItems().add(el.getType() + " attachment " + i);
                }

                attachmentsListView.setOnMouseClicked(event1 -> {
                });
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
            }

        }));

    }

    @FXML
    void onQuitAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void onWriteAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("SendMessageWindow.fxml"));
        Stage stage = new Stage();
        stage.setTitle("SMTP Example");
        stage.setScene(new Scene(root,800, 600));
        stage.show();
    }

}
