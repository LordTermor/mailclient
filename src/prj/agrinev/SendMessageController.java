package prj.agrinev;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;

public class SendMessageController {

    SMTPClient client;
    @FXML
    private TextField toTextBox;
    @FXML
    private TextField subjectTextBox;
    @FXML
    private TextArea bodyTextBox;

    @FXML
    private ListView<String> attachmentPathsListView;

    {
        try {
            client = new SMTPClient(Main.SMTP_HOSTNAME, Main.SMTP_PORT);
            client.setAuthData(Main.NAME, Main.PASS);
        } catch (Exception e) {

        }
    }

    @FXML
    void onSendButtonAction(ActionEvent event) {
        try {
            SMTPClient.Message msg = client.write()
                    .to(toTextBox.getText())
                    .subject(subjectTextBox.getText())
                    .body(bodyTextBox.getText());
            for (String el :
                    attachmentPathsListView.getItems()) {
                msg.attach(el);
            }
            msg.send();
            toTextBox.clear();
            subjectTextBox.clear();
            bodyTextBox.clear();
            attachmentPathsListView.getItems().clear();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }


    }

    @FXML
    void onAttachButtonAction(ActionEvent event) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Attachment File");
            File choosed = fileChooser.showOpenDialog(attachmentPathsListView.getScene().getWindow());
            attachmentPathsListView.getItems().add(choosed.getAbsolutePath());
        } catch (Exception e) {

        }
    }
}
