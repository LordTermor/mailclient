package prj.agrinev;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.concurrent.TimeoutException;

public class SMTPClient {
    SSLSocket socket;
    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();
    private String lastMessage;
    private PrintWriter socketOutputStream;
    private BufferedReader socketInputStream;

    private String login;
    private String password;

    private String host;

    private int port;

    SMTPClient(String host, int port) throws IOException {
        this.host = host;
        this.port = port;
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            return null;
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

    private static String getBase64ContentsOfFile(File att) throws IOException {
        byte[] bytes = loadFile(att);

        return Base64.getEncoder().encodeToString(bytes);
    }

    public void setAuthData(String login, String password) {
        this.login = login;
        this.password = password;

    }

    private String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    private void socketSend(String message, boolean waitForResponse) throws IOException {
        System.out.print("SMTP: CLIENT: "+message);
        socketOutputStream.write(message);
        socketOutputStream.flush();
        if (waitForResponse) {
            String str;
            try {
                while ((str = socketInputStream.readLine())!=null){
                    lastMessage = str;
                    System.out.println("SMTP: SERVER: "+lastMessage);
                }
            }catch (Exception e){
            }

        }

    }

    private void socketSend(String message) throws IOException {
        socketSend(message, true);

    }

    Message write() {
        Message msg = new Message();
        return msg;
    }

    private void initSocket() throws IOException {
        socket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(host, port);
        socket.startHandshake();
        socketOutputStream = new PrintWriter(socket.getOutputStream(), true);
        socketInputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        socket.setSoTimeout(200);
        socketSend("EHLO " + socket.getInetAddress().getHostName() + "\r\n");
        socketSend("AUTH LOGIN\r\n");

        socketSend(Base64.getEncoder().encodeToString(login.getBytes()) + "\r\n");
        socketSend(Base64.getEncoder().encodeToString(password.getBytes()) + "\r\n");
    }

    class Message {
        private String mTo;
        private String mSubject;
        private String mBody;
        private ArrayList<String> mAttachments;

        public Message() {
            mAttachments = new ArrayList<>();
        }


        public Message to(String pTo) {
            this.mTo = pTo;
            return this;
        }

        public Message subject(String pSubject) {
            this.mSubject = pSubject;
            return this;
        }

        public Message body(String pBody) {
            this.mBody = pBody;
            return this;
        }

        public Message attach(String pAttachment) throws FileNotFoundException {
            if (!new File(pAttachment).exists()) {
                throw new FileNotFoundException();
            }
            this.mAttachments.add(pAttachment);
            return this;
        }

        public Message attach(ArrayList<String> pAttachments) throws FileNotFoundException {
            for (String element :
                    pAttachments) {
                if (!new File(element).exists()) {
                    throw new FileNotFoundException();
                }
            }

            this.mAttachments.addAll(pAttachments);
            return this;
        }

        public void send() throws IOException {

            initSocket();

            socketSend("MAIL FROM:<" + login + ">\r\n");
            socketSend("RCPT TO:<" + mTo + ">\r\n");
            socketSend("DATA\r\n");

            String boundary = randomString(12);

            socketSend("Subject: " + mSubject + "\r\n", false);
            socketSend("MIME-Version: 1.0\r\n", false);
            socketSend("Content-Type:multipart/mixed;boundary=\"" + boundary + "\"\r\n", false);
            socketSend("--" + boundary + "\r\n", false);
            socketSend("Content-Type: text/plain\r\n\r\n", false);
            socketSend(mBody + "\r\n\r\n", false);

            for (String attachment :
                    mAttachments) {
                File att = new File(attachment);
                socketSend("--" + boundary + "\r\n", false);
                socketSend("Content-Type:application/octet-stream;name=\"" + att.getName() + "\"\r\n", false);
                socketSend("Content-Transfer-Encoding:base64\r\n", false);
                socketSend("Content-Disposition:attachment;filename=\"" + att.getName() + "\"\r\n\r\n", false);
                socketSend(getBase64ContentsOfFile(att) + "\r\n\r\n", false);
            }

            socketSend("--" + boundary + "--\r\n.\r\n");

            socketSend("QUIT\r\n");

        }

    }

}
